<?php

namespace xolodok\status;

use Yii;
use yii\base\Action;
use yii\web\Response;
use yii\base\InvalidConfigException;
use yii\web\NotFoundHttpException;

/**
 * Class StatusAction
 * @package xolodok\status
 */
class StatusAction extends Action
{
    /**
     * @var Model class.
     */
    public $modelClass;

    /**
     * @var Messages for attribute status.
     */
    public $messages = [];

    /**
     * @var Default messages for attribute status.
     */
    private $defaultMessages = [];

    /**
     * @inheritdoc.
     */
    public function init()
    {
        parent::init();

        if($this->modelClass === null){
            throw new InvalidConfigException('The "modelClass" property must be set.');
        }

        $this->defaultMessages = [
            Yii::t('app', 'Not Active'),
            Yii::t('app', 'Active')
        ];
    }

    /**
     * @inheritdoc.
     */
    public function run($id, $attr)
    {
        if(Yii::$app->request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = $this->findModel($id);
            $model->$attr = 1 - $model->$attr;

            $message = isset($this->messages[$attr]) ? $this->messages[$attr][$model->$attr] : $this->defaultMessages[$model->$attr];

            return [
                'success' => $model->save(),
                'status' => $model->$attr,
                'message' => $message,
            ];
        }
    }

    /**
     * Finds the model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Model the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $args = [$id];
        $model = call_user_func_array([$this->modelClass, 'findOne'], $args);
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}