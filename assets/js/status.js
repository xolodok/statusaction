$(function(){
    $('body').on('click', '.change-status', function(){
        var button = $(this);
        $.ajax({
            url: $(this).data('action'),
            data: {id: $(this).data('id'), attr: $(this).data('attr')},
            success: function(result){
                if(result.success){
                    if(result.status){
                        button.removeClass('btn-danger').addClass('btn-success').text(result.message);
                    }
                    else{
                        button.removeClass('btn-success').addClass('btn-danger').text(result.message);
                    }
                }
            },
            error: function(){
            }
        });
    });
});
