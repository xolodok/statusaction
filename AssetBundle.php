<?php

namespace xolodok\status;

/**
 * Class AssetBundle
 * @package xolodok\status
 */
class AssetBundle extends \yii\web\AssetBundle
{
    /**
     * @var array
     */
    public $css = [
    ];

    /**
     * @inherit
     */
    public $js = [
        'js/status.js',
    ];

    /**
     * @var array
     */
    public $depends = array(
        'yii\web\JqueryAsset'
    );

    /**
     * @inheritdoc.
     */
    public function init()
    {
        $this->sourcePath = __DIR__ . '/assets';

        parent::init();
    }
}