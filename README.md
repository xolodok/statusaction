Status action
=============
Status action

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist xolodok/yii2-action-status "*"
```

or add

```
"xolodok/yii2-action-status": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Attaching this actions to the model (Post in the example) :

```
public function actions()
{
    return [
        'change-status' => [
            'class' => 'xolodok\status\StatusAction',
            'modelClass' => Post::className(),
            'messages' => [
                'active' => [
                    Yii::t('app', 'Not active'),
                    Yii::t('app', 'Active')
                ]
            ],
        ],
    ];
}
```
You need a register in view AssetBundle for example:

```
<?php \xolodok\status\AssetBundle::register($this); ?>
```

Status button widget example :

```
echo \xolodok\status\widgets\StatusButton::widget([
        'modelId' => $model->id,
        'value' => $model->attribute,
        'statusMessages' => [
            Yii::t('app', 'Not active'),
            Yii::t('app', 'Active')
        ]
    ]);
```
