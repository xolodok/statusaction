<?php

namespace xolodok\status\widgets;

use yii\base\InvalidConfigException;
use yii\helpers\Url;

/**
 * Class StatusButton
 * @package xolodok\status
 */
class StatusButton extends \yii\base\Widget
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @var string Status attribute.
     */
    public $attribute = 'active';

    /**
     * @var int Model ID.
     */
    public $modelId;

    /**
     * @var int Current status value.
     */
    public $value;

    /**
     * @var array Status types.
     */
    public $statusTypes = [
        'btn-danger',
        'btn-success',
    ];

    /**
     * @var array Status messages.
     */
    public $statusMessages = [];

    /**
     * @inheritdoc.
     */
    public function init()
    {
        parent::init();

        if(!is_int($this->modelId)){
            throw new InvalidConfigException('The "ModelId" property must be integer.');
        }

        if($this->value !== self::STATUS_INACTIVE && $this->value !== self::STATUS_ACTIVE){
            throw new InvalidConfigException('The "value" property must be 0 or 1.');
        }

        if(empty($this->statusMessages)){
            $this->statusMessages = [
                \Yii::t('status', 'Not active'),
                \Yii::t('status', 'Active')
            ];
        }
    }

    /**
     * @inheritdoc.
     */
    public function run()
    {
        return \Yii\helpers\Html::button($this->getStatusText(), [
            'class' => 'change-status btn btn-xs ' . $this->getStatusClass(),
            'data-id' => $this->modelId, 
            'data-attr' => $this->attribute,
            'data-action' => Url::to(['change-status'])
        ]);
    }

    /**
     * Return current status button type
     * @return string.
     */
    private function getStatusClass()
    {
        return $this->statusTypes[$this->value];
    }

    /**
     * Return current status button text
     * @return string.
     */
    private function getStatusText()
    {
        return $this->statusMessages[$this->value];
    }
}